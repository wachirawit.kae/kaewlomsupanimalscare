import React, { Component } from "react";
import "./app.css";

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <header className="container">
          <nav className="navbar navbar-expand-lg navbar-light navbar-inverse row">
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarNavDropdown"
              aria-controls="navbarNavDropdown"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>
            <section
              className="collapse navbar-collapse"
              id="navbarNavDropdown"
            >
              <ul className="navbar-nav">
                <li className="nav-item">
                  <a className="nav-link" href="/">
                    Home
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    Booking
                  </a>
                </li>
                <li className="nav-item dropdown">
                  <a
                    className="nav-link navbar-toggle"
                    href="#"
                    id="navbarDropdownMenuLink"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Shopping
                  </a>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="navbarDropdownMenuLink"
                  >
                    <a className="dropdown-item" href="/shopping/">
                      Foods
                    </a>
                    <a className="dropdown-item" href="#">
                      Services
                    </a>
                  </div>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    About
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    Contact
                  </a>
                </li>
              </ul>
            </section>
            {account()}
            <i className="fas fa-shopping-cart" />
          </nav>
          <section className="underNavBar row justify-content-around mb-3">
            <h3 className="navbar-brand">Animal Care</h3>
            <div className="mt-2">
              <input type="text" placeholder="Search.." name="search" />
              <span type="search">
                <i className="fa fa-search" />
              </span>
            </div>
          </section>
        </header>
        {this.props.children}
        <footer>
          <section className="row">
            <ul className="col-sm-4">
              <p>Get started</p>
              <li>Home</li>
              <li>Sign up</li>
            </ul>
            <ul className="col-sm-4">
              <p>About us</p>
              <li>Company Information</li>
              <li>Contact us</li>
              <li>Reviews</li>
            </ul>
            <ul className="col-sm-4">
              <p>Support</p>
              <li>FAQ</li>
              <li>Help desk</li>
              <li>Forums</li>
            </ul>
          </section>
          <section className="row">
            <p>Animals care</p>
            <br />
            <i className="fab fa-facebook-f" />
            <i className="fab fa-twitter" />
            <i className="fab fa-google-plus-g" />
          </section>
        </footer>
      </React.Fragment>
    );
  }
}

function account() {
  const account = false; //check have account?

  if (account)
    return (
      <a className="nav-link disabled" href="#">
        Name LastName
      </a>
    );
  else
    return (
      <div className="nav-item dropdown">
        <a
          className="nav-link disabled"
          href="#"
          id="navbarDropdownMenuLink"
          role="button"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          My Account
        </a>
        <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a className="dropdown-item" href="/users/">
            Register
          </a>
          <a className="dropdown-item" href="/users/login/">
            Login
          </a>
        </div>
      </div>
    );
}
