const express = require("express");
const os = require("os");
const path = require("path");

const app = express();

app.use(express.static("dist"));
app.use("/static", express.static(path.join(__dirname, "src")));
app.get("/api/getUsername", (req, res) =>
  res.send({ username: os.userInfo().username })
);
app.listen(8080, () => console.log("Listening on port 8080!"));
